/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.son.oxswing;

import java.io.Serializable;

/**
 *
 * @author mc-so
 */
public class Player implements Serializable{

    private char name;

    public int getLose() {
        return lose;
    }

    public void lose() {
        lose++;
    }

    public int getDraw() {
        return draw;
    }

    public void draw() {
        draw++;
    }

    public int getWin() {
        return win;
    }

    public void win() {
        win++;
    }
    private int lose=0;
    private int draw=0;
    private int win=0;

    Player(char name) {
        this.name = name;
    }

    public char getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Player{" + "name=" + name + ", lose=" + lose + ", draw=" + draw + ", win=" + win + '}';
    }
    
}
